//we armi el aghani el bel 3arabi
//artist-esmeloghnia
let express = require('express');
const app = express();
const router  = express.Router();
const bodyParser = require('body-parser');

var fs = require('fs');
var request = require('sync-request');
var english = /^[A-Za-z0-9\-_\'\s]*$/;
const tabletserver1_ip = 'http://localhost:5000';
const tabletserver2_ip = 'http://localhost:5001';

const readline = require('readline');
readline.emitKeypressEvents(process.stdin);
//process.stdin.setRawMode(true);

/*
Initializing
*/
app.use( bodyParser.json({limit: '50mb'}) );
app.use( bodyParser.urlencoded({ extended: true }));
app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
  res.header("Access-Control-Allow-Origin", req.headers.origin);
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,identifier");
  res.header("Access-Control-Allow-Credentials",true);
  next();
});

app.use('/server', router);


/*
first: read the json data set file and generate a bigTable format into a map
*/
let tracks = fs.readFileSync("./tracks.json");
tracks = JSON.parse(tracks.toString());
tracks.sort(function(a, b){return a.artist_name.localeCompare(b.artist_name);});

// create map (bigTable)
let tracks_sorted = {};
for (let i in tracks){
  let current_row_key = (tracks[i].artist_name + " " + tracks[i].track_name).split(' ').join('-');
  if (!(current_row_key in tracks_sorted) && english.test(current_row_key)) //prevent duplicate adding (overwriting)
    tracks_sorted[current_row_key] = {
      row_key: (tracks[i].artist_name + " " + tracks[i].track_name).split(' ').join('-'),
      artist_name: tracks[i].artist_name.split(' ').join('-'),
      track_name: tracks[i].track_name.split(' ').join('-'),
      album_name: tracks[i].album_name.split(' ').join('-'),
      track_rating: tracks[i].track_rating
    };
}

// =========================================================================
//get artists count, songs count, and strange characters count
let numbers = Object.keys(tracks_sorted);
let artist_count = 1, strange_count = 0, first_char_count = 1;
for (let i = 0 ; i < numbers.length - 1; i++){
  let current_row = tracks_sorted[numbers[i]], next_row = tracks_sorted[numbers[i+1]];
  if (current_row.artist_name != next_row.artist_name)
    artist_count++;
  if (!english.test(current_row.row_key))
    strange_count++;
  if (current_row.artist_name[0].toLowerCase() != next_row.artist_name[0].toLowerCase())
    first_char_count++;
}

console.log(artist_count); //1585
console.log(strange_count);//10006
console.log(numbers.length);
console.log(first_char_count);
for (let i = 0 ; i < numbers.length; i++){
  //console.log(numbers[i]); //show row_keys
}
// =========================================================================
let tablets = [], arr = [];
//divide the map into {first_char_count} tablets (29 tablets according to our data)
for (let i = 0 ; i < numbers.length - 1; i++){
  let current_row = tracks_sorted[numbers[i]], next_row = tracks_sorted[numbers[i+1]];
  if (current_row.artist_name[0].toLowerCase() == next_row.artist_name[0].toLowerCase()){
    arr.push(current_row);
  } else {
    arr.push(current_row);
    tablets.push(arr);
    arr = [];
  }
} arr.push(tracks_sorted[numbers[numbers.length - 1]]); tablets.push(arr);
// =========================================================================
// join tablets into 2 groups, according to alphabetic arrangement
// and create Metadata => metadata.first_char, metadata.serverIp
let metadata = {};
let server_tablets1 = []
let server_tablets2 = []
let i = 0;
for (; i < tablets.length / 2; i++){
  server_tablets1 = server_tablets1.concat(tablets[i]);
  //console.log(tablets[i][0][Object.keys(tablets[i][0])[0]]); //da bagib awel row_key fi awel entry fi kol tablet, 3shan l satr el gy agib awel 7arf fih
  metadata[ tablets[i][0][Object.keys(tablets[i][0])[0]] [0].toLowerCase() ] = tabletserver1_ip;
  if (server_tablets1.length > numbers.length/2) {i++; break;} //to evenly divide tablets on servers
} for (; i < tablets.length; i++){
  server_tablets2 = server_tablets2.concat(tablets[i]);
  metadata[ tablets[i][0][Object.keys(tablets[i][0])[0]] [0].toLowerCase() ] = tabletserver2_ip;
}
console.log(server_tablets1.length + server_tablets2.length);
//console.log(metadata);
// =========================================================================
// send to tablet servers
try {
  console.log("* sending " + server_tablets1.length + " row to tablet server 1");
  let res = request( 'POST', tabletserver1_ip + '/tablet/init', {
    headers: {
      'source': 'main-server'
    },
    json: server_tablets1
  });
} catch (err) { console.log("\x1b[31m%s\x1b[0m", "* couldn't connect to tablet server 1 at " + tabletserver1_ip);}
try {
  console.log("* sending " + server_tablets2.length + " row to tablet server 2");
  let res2 = request( 'POST', tabletserver2_ip + '/tablet/init', {
    headers: {
      'source': 'main-server'
    },
    json: server_tablets2
  });
} catch(err){ console.log("\x1b[31m%s\x1b[0m", "* couldn't connect to tablet server 2 at " + tabletserver2_ip);}
//console.log(res);
// =========================================================================
/**
	 * RECEIVE 'WHERE IS' REQUEST
   * this function doesn't identify if row exists or not, just checks the range
	 * @param {string} req.body.row_key
   * @return {string} tablet server port
   */
router.post("/where-is", function (req, res) {
  let reply;
  if (req.body.row_key[0].toLowerCase() in metadata)
    reply = metadata[ req.body.row_key[0].toLowerCase() ];
  else
    reply = tabletserver1_ip; //special characters and numbers are to be put in server1
  res.status(200).send({"server-location": reply});
});
// ------------------------------------------
/**
	 * RECEIVE 'UPDATE' REQUEST
   * updates dirty data and edits metadata table accordingly
	 * @param {map} req.body.data
   * @param {string} req.body.source
   */
router.post("/update", function (req, res) {
  let keys = Object.keys(req.body.data);
  for (let i = 0; i < keys.length; i++){
    if (req.body.data[keys[i]] == null) //case it was a delete in tablet server
      delete tracks_sorted[keys[i]];
    else {
      tracks_sorted[keys[i]] = req.body.data[keys[i]];
      if (!(keys[i][0] in metadata) ){
        if (req.body.source == '1') metadata[keys[i][0]] = tabletserver1_ip;
        if (req.body.source == '2') metadata[keys[i][0]] = tabletserver2_ip;
      }
    }
  }
  console.log("* updated " + keys.length + " rows");
  res.status(200).send({"status": "successfuly updated"});
});
// ------------------------------------------


process.stdin.on("keypress", function(chunk, key) {
  if(key.name === "p") {
    console.log("* writing data to file .. ");
    fs.writeFileSync("./mydata", JSON.stringify(tracks_sorted), function(err) {
      if(err) { return console.log(err); }
    });
    console.log("* finished");
  }
});



/*
SERVER INIT
*/
const PORT = process.env.PORT || 3000;
app.listen(PORT, err => {
  if (err) {
    console.error(err);
  } else {
    console.log(`* master-server listening to port: ${PORT}`);
  }
});
