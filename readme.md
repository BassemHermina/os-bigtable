download the data set [here](https://drive.google.com/open?id=1VK-wQ_WcR3Eu9Ryl0QP9lDO5ttt7v6er).

## definitions
- All the data is in one **BigTable** stored in a **Main-server**.
- The table is in **3-Dimension** as we define data by (Row-key, Column-key, time-stamp).
- The table is sorted **alphabetically** by **row-key**, nothing fancy.
- The table is divided into small **tablets**, each tablet contain a small range of **row-keys** of the table [ex: tablet1 {rows starting with a -> rows starting with d}, tablet2 {rows starting with e -> rows starting with j}, ..].
- **Main-server** divides the bigTable into smaller tablets and keeps track of their row range and number.
- **Main-server** is responsible for assigning tablets to **tablet-servers**, each tablet server have many tablets assigned to it.
- **Clients** can communicate directly with tablet servers, which means client data **DON'T** move through the main-server.
- Locations (ip,port,filename,...) of all tablet-servers is stored in a **Metadata** table in the main-server.

## flow
- In the beginning the **Master-server** contains all the data.
- The **master** should divide tablets and assign to tablet servers updating the metadata table (done only once).

> - How does the master **move** the data to the tablet servers?

> - What is the form of the **Metadata** table? _it think it should contain, for every tablet, it's row range, it's assigned tablet server location, so it's tablet should have one entry in the Metadata table._

- Now, after each tablet-server having received it's tablets/data, they all should **listen to client requests**.

- When a client requests a certain query using the row key it sends to **master** asking which tablet server to access.

- Master replies to the requesting client with the **tablet server** number.

> What is the _tablet server number_ ? is it the ip/port? or does every client know each tablet server address and id?

> And what if it was a case that needed data from multiple tablet servers?

- Client sends its request to the designated tablet server.

- If the query involves any write operations (set or delete), the tablet server has to be locked (No other client can access this tablet server) till these operations finish. [Atomic operations]

> How to implement atomic lock and edit operations in JS?

- Tablet server performs the operations requested by the client.

- Periodically each tablet server writes its tablets to the original data tables. (It is smart to only write updated data).

> That means there should be a timer/alarm/thread that fires a write-to-main-server event which writes only 'Dirty' data to the server. there shouldn't be conflicts as every tuple is written only once in one tablet server.

- If a client requested a locked tablet server, it gets blocked.

- If a client requested a query that adds a row that will affect the tablet server row key range, the tablet server should update the metadata table accordingly.

> That means that every tablet server should be able to edit the Metadata table in the Main server, wouldn't that introduce conflicts?

## Master Server Todo:
- [x] create the bigTable from the JSON data (sort alphabetically)
- [x] divide the bigTable into tablets and divide the tablets into 2 groups
- [x] create the Metadata table and save the tablets ranges, and input the 2 tablet servers ip/port from server administrator (e7na :D)
- [x] send data to **tablet servers** using the tablet server /receive end point?
- [x] listen to **clients** query requests on a specified end point, and reply with the locations of data (from the Metadata table)
- [x] listen to **tablet server** call updating the edited tuples.
- [x] listen to **tablet server** call updating the Metadata table.

## Tablet Server Todo:
- [x] receive the tablets assigned to this server on the /receive end point from **master**.
- [x] listen to **clients** query requests on a specified end point (set/delete/add/read), and implementing their functionalities.
- [x] _getting-blocked-while-updating_ functionality.
- [x] keep track of updated tuples in tablet.
- [x] periodically send to master to update the dirty tuples on special /update route in **master**.
- [x] send to **master** to update the Metadata table if a client add/deleted a row.
- [x] test all data/functionality of client requests

## Client Todo:
- [x] request data location from **master server**
- [x] _getting-blocked-if-tablet-server-was-blocked_ functionality.
- [x] _getting-unblocked-when-tablet-server-finishes_ functionality.
- [x] set() query
- [x] deleteCells() query
- [x] deleteRow() query
- [x] addRow() query
- [x] readRow() query
