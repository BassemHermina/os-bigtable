let express = require('express');
const app = express();
const router  = express.Router();
const bodyParser = require('body-parser');
var request = require('sync-request');
var fs = require('fs');

const masterserver_ip = 'http://localhost:3000';
const me = '1'
const readline = require('readline');
readline.emitKeypressEvents(process.stdin);

// TODO: create middleware for logging
// TODO: request updates in metadata in main-server (for /add-row, /delete-row only)
//        we eftker en mmkn l row ykon bade2 be special character, sa3etha ha7ot row gdid fi table l metadata


/*
MIDDLEWARES
*/
// ==================================================================
// don't limit body size as to receive the db
app.use( bodyParser.json({limit: '50mb'}) );
app.use( bodyParser.urlencoded({ extended: true }));
app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
  res.header("Access-Control-Allow-Origin", req.headers.origin);
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,identifier");
  res.header("Access-Control-Allow-Credentials",true);
  next();
});

/**
	 * checks if this tablet server have already received the data or if this
   * is the data being sent
	 *
	 * @param {header} source : "main-server"
   * @param {boolean} globals.have_data shows if tablet-server have data or not
	 */
const serverVerifier = function(req, res, next) {
  if (globals.have_data && req.get("source")=="main-server"){
    console.log('\x1b[33m%s\x1b[0m', "* already received data before from main-server.");
    res.status(500).send()
  }
  else if (!globals.have_data && req.get("source")=="main-server")
    next();
  else {
    console.log('\x1b[33m%s\x1b[0m', "* not authorized to access this end point.");
    res.status(500).send({"status": "error"});
  }
};

/**
	 * checks if row_key provided in req.body is available (not a wrong row_key)
	 * used for set / delete-cells / delete-row / read-row
	 * @param {string} req.body.row_key
   * @param {string} req.body.data the JSON object if exists
	 */
const m1 = function (req, res, next){
  if ( !(req.body.row_key in db) || req.body.row_key==null){
    console.log("\x1b[31m%s\x1b[0m", "* error, row_key not found!");
    res.status(200).send({"status": "Error, row not found"});
  } else
    next();
}

console.log("* Initializing tablet-server. waiting for main-server data ...");
let globals = {
  have_data: false
};
//===================================================================

let db = {};
let updated = {};
//var map = {};
// add a item
//map[key1] = value1;
// or remove it
//delete map[key1];
// or determine whether a key exists
//key1 in map;


/*
ROUTES
*/
// //===================================================================
app.use('/tablet', router);
// ------------------------------------------
/**
	 * RECEIVE DATA FROM DATABASE
	 * @param {JSON array} req.body array of JSON objects, containing all data (row as array entry)
	 */
router.post("/init",serverVerifier, function (req, res) {
  console.log("* receiving data from main-server ..");
  for (let i in req.body)
    db[req.body[i]['row_key']] = req.body[i];
  console.log("* finished receiving " + req.body.length + " rows from main-server.");
  globals.have_data = true;
  res.status(200).send({"status": "success"});
});
// ------------------------------------------

/**
	 * RECEIVE 'WRITE CELLS IN A ROW' REQUEST
   * add the cells to the desired row
   * save the new row into {updated}
	 * @param {string} req.body.row_key
   * @param {object} req.body.data containing cells
   > DOCUMENT SNIPPET: "Input: row key, [column family: (qualifier) and cell data]-> can be repeated n times"
   */
router.post("/set", m1, function (req, res) {
  let keys = Object.keys(req.body.data);
  for (let i = 0; i < keys.length; i++){
    db[req.body.row_key][keys[i]] = req.body.data[keys[i]];
  }
  updated[req.body.row_key] = db[req.body.row_key];
  console.log("* row updated");
  res.status(200).send({"status": "row updated"});
});
// ------------------------------------------

/**
	 * RECEIVE 'DELETE SOME CELLS IN A ROW' REQUEST
   * remove the cells from the desired row
   * save the new row into {updated}
	 * @param {string} req.body.row_key
   * @param {array} req.body.data
   > DOCUMENT SNIPPET: "Input: row key, [column family: (qualifier)]-> can be repeated n times"
   */
router.post("/delete-cells", m1, function (req, res) {
  for (let i = 0; i < req.body.data.length; i++){
    delete db[req.body.row_key] [req.body.data[i]];
  }
  updated[req.body.row_key] = db[req.body.row_key];
  console.log("* row updated");
  res.status(200).send({"status": "row updated"});
});
// ------------------------------------------

/**
	 * RECEIVE 'DELETE A ROW' REQUEST
   * delete the desired row
   * save the new row into {updated}
	 * @param {string} req.body.row_key
   > DOCUMENT SNIPPET: "Input: row key"
   */
router.post("/delete-row", m1, function (req, res) {
  delete db[req.body.row_key];
  updated[req.body.row_key] = null;
  console.log("* row deleted");
  res.status(200).send({"status": "row updated"});
});
// ------------------------------------------

/**
	 * RECEIVE 'ADD A ROW' REQUEST
   * add a new row to db
   * save the new row into {updated}
	 * @param {string} req.body.row_key
   * @param {array} req.body.data
   > DOCUMENT SNIPPET: "Input: row key,  [column family: (qualifier) and cell data]-> can be repeated n times"
   */
router.post("/add-row", function (req, res) {
  db[req.body.row_key] = req.body.data;
  updated[req.body.row_key] = db[req.body.row_key];
  console.log("* row created");
  res.status(200).send({"status": "row updated"});
});
// ------------------------------------------

/**
	 * RECEIVE 'READ ROW' REQUEST
	 * @param {Number array} req.body.row_keys
   > DOCUMENT SNIPPET: "Input: [row key] -> repeated n times"
   */
router.post("/read-rows", function (req, res) {
  let data = [];
  for (let i = 0; i < req.body.row_keys.length; i++){
    data.push(db[req.body.row_keys[i]]);
  }
  console.log("* returning " + req.body.row_keys.length + " rows");
  res.status(200).send(data);
});
//===================================================================

//trackname, albumname, artistname, genres

// fi 7aga ghalat fl body hna
setInterval(myTimer, 10000);
function myTimer() {
  try {
    console.log("* requesting to update " + Object.keys(updated).length + " rows in master");
    let res = request( 'POST', masterserver_ip + '/server/update', {
      json: {'data': updated, 'source': me}
    });
  } catch (err) { console.log("\x1b[31m%s\x1b[0m", "* couldn't connect to master " + masterserver_ip);}

  // clear updated
  updated = {};
}


//testing blocking the client sending request if server was blocked: SUCCESS!
router.get("/busy", function (req, res) {
  console.log("inf");
  let x;
  while(true) x = 2
  res.status(200).send({"status": "row updated"});
});

process.stdin.on("keypress", function(chunk, key) {
  if(key.name === "p") {
    console.log("* writing data to file .. ");
    fs.writeFileSync("./mydata", JSON.stringify(db), function(err) {
      if(err) { return console.log(err); }
    });
    console.log("* finished");
  }
});



/*
SERVER INIT
*/
const PORT = process.env.PORT || 5001;
app.listen(PORT, err => {
  if (err) {
    console.error(err);
  } else {
    console.log(`* tablet-server listening to port: ${PORT}`);
  }
});
