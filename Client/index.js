var fs = require('fs');
var request = require('sync-request');

const masterserver_ip = 'http://localhost:3000'
const tabletserver1_ip = 'http://localhost:5000';
const tabletserver2_ip = 'http://localhost:5001';

function whereis(row_key){
  try {
    console.log("* sending where-is '" + row_key + "' to master.");
    let res = request( 'POST', masterserver_ip + '/server/where-is', {
      json: {'row_key': row_key}
    });
    let d = JSON.parse(res.getBody('utf8'));
    return d['server-location'];
  } catch (err) {
    console.log("\x1b[31m%s\x1b[0m", "* couldn't connect to master at " + masterserver_ip);
    return null;
  }
}

//row_key: string
//cells: object
function query_setCells(row_key, cells){
  try {
    let tabletloc = whereis(row_key);
    if (!tabletloc) throw Error("error happened reading place of data")
    console.log("* sending 'set-cells' for '" + row_key + "' @ " + tabletloc);
    let res = request( 'POST', tabletloc + '/tablet/set', {
      json: {'row_key': row_key, 'data': cells}
    });
    console.log(JSON.parse(res.getBody('utf8')).status);

  } catch (err) {
    console.log("\x1b[31m%s\x1b[0m", "* couldn't connect to master at " + masterserver_ip);
  }
}
//row_key: string
//cells: array
function query_deleteCells(row_key, cells){
  try {
    let tabletloc = whereis(row_key);
    if (!tabletloc) throw Error("error happened reading place of data")
    console.log("* sending 'delete-cells' for '" + row_key + "' @ " + tabletloc);
    let res = request( 'POST', tabletloc + '/tablet/delete-cells', {
      json: {'row_key': row_key, 'data': cells}
    });
    console.log(JSON.parse(res.getBody('utf8')).status);

  } catch (err) {
    console.log("\x1b[31m%s\x1b[0m", "* couldn't connect to master at " + masterserver_ip);
  }
}
function query_deleteRow(row_key){
  try {
    let tabletloc = whereis(row_key);
    if (!tabletloc) throw Error("error happened reading place of data")
    console.log("* sending 'delete-row' for '" + row_key + "' @ " + tabletloc);
    let res = request( 'POST', tabletloc + '/tablet/delete-row', {
      json: {'row_key': row_key}
    });
    console.log(JSON.parse(res.getBody('utf8')).status);

  } catch (err) {
    console.log("\x1b[31m%s\x1b[0m", "* couldn't connect to master at " + masterserver_ip);
  }
}
function query_addRow(row_key, object){
  try {
    let tabletloc = whereis(row_key);
    if (!tabletloc) throw Error("error happened reading place of data")
    console.log("* sending 'add-row' at '" + row_key + "' @ " + tabletloc);
    let res = request( 'POST', tabletloc + '/tablet/add-row', {
      json: {'row_key': row_key, 'data': object}
    });
    console.log(JSON.parse(res.getBody('utf8')).status);

  } catch (err) {
    console.log("\x1b[31m%s\x1b[0m", "* couldn't connect to master at " + masterserver_ip);
  }
}
function query_readRows(row_keys){
try {
  let tabletloc = {};
  for (let i = 0; i < row_keys.length; i++){
    let loc = whereis(row_keys[i]);
    if (!loc) throw Error("error happened reading place of data")
    if (!(loc in tabletloc)) tabletloc[loc] = [];
    tabletloc[loc].push(row_keys[i]);
  }
  console.log("* sending 'read-rows' for '" + row_keys + "' @ " + tabletloc);
  console.log(tabletloc);
  let keys = Object.keys(tabletloc);
  for (let i = 0; i < keys.length; i++){
    let res = request( 'POST', keys[i] + '/tablet/read-rows', {
      json: {'row_keys': tabletloc[keys[i]] }
    });
    console.log(JSON.parse(res.getBody('utf8')));
  }

} catch (err) {
  console.log(err);
  console.log("\x1b[31m%s\x1b[0m", "* couldn't connect to master at " + masterserver_ip);
}
}
query_addRow('1', {'7aamada':'rawa7 nam', '7amadatani':'ra7w geh'});
//query_deleteCells('5-Seconds-of-Summer-Good-Girls', ['artist_name', 'track_name', 'album_name']);
//query_setCells('5-Seconds-of-Summer-Good-Girls', {'5alty': 'bteghsel'});
//query_deleteRow('5-Seconds-of-Summer-Good-Girls');
//query_deleteRow('5-Seconds-of-Summer-Good-Girls');

query_readRows(['1', '5-Seconds-of-Summer-Good-Girls', "Kanita-Don't-Let-Me-Go"])
